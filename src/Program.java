import java.io.IOException;
import java.io.UncheckedIOException;

/**
 * Created by Роман on 31.03.2017.
 */
public class Program {

    public static void main(String[] args) {

        try {
            new Program().throwable();
        }catch(Exception e){
            System.out.println("Exception: ");
            System.out.println(e.toString());
            System.out.println("Cause of exception: ");
            System.out.println(e.getCause().toString());
            System.out.println("Suppressed exception: ");
            System.out.println(e.getSuppressed().toString());
        }
    }

    void throwable() throws Exception {
        try(Worker worker = new Worker()){
            worker.work();
        }catch (IOException | UncheckedIOException e){
            e.initCause(new MyError());
            throw e;
        }
    }
}
